require 'date'
require 'fileutils'
require 'logger'
require 'selenium-webdriver'
require 'time'
require 'yaml'

# Setup
config = YAML.load_file('config.yml')
p "実行モード：#{config['mode']}"
FileUtils.mkdir_p(config['log_dir']) unless FileTest.exist?(config['log_dir'])
log = Logger.new(config['log_dir'] << Date.today.to_s << '.log')

# Browser setup
options = Selenium::WebDriver::Firefox::Options.new
options.add_preference('permissions.default.image', 2)
driver = Selenium::WebDriver.for(:firefox, options: options)

# ページを開いて待つ
driver.get(config['product_url'])
driver.execute_script(
  'document.querySelector("button.checkout").scrollIntoView({block: "center"});'
)
# TODO: configで切り替えられるように
# 商品タイプの設定（色とか）
# driver.find_elements(:name, 'inventory_id')[1].click
log.info('Setup OK')

# 購入処理
begin
  purchase_btn = driver.find_elements(:class, 'checkout')[1]
  wait = Selenium::WebDriver::Wait.new(timeout: 60, interval: 0.1)
  wait.until { purchase_btn.enabled? }

  # 開始時間まで購入ボタン連打
  while driver.current_url == config['product_url']
    purchase_btn.click
    p "#{Time.now.iso8601(3)}:購入ボタン連打中"
  end
  log.info('買い物かごに入れました。')

  driver.find_element(:class, 'purchaseButton').click
  log.info('購入手続きへ進みます。')

  driver.find_element(:name, 'u').send_key(config['login']['user'])
  driver.find_element(:name, 'p').send_key(config['login']['password'])
  driver.find_element(:id, 'login_submit').submit
  log.info('ログインしました。')

  # TODO: seleniumで取れるタイトルやURLと乖離があるため判定できない
  # wait.until { driver.find_element(:id, 'submit-button').enabled? }
  # driver.find_element(:id, 'submit-button').click
  # log.info('お届け先を選択しました。')

  wait.until { driver.find_element(:name, 'commit').enabled? }
  driver.find_element(:name, 'commit').click if config['mode'] == 'prod'
  log.info('注文しました。')
rescue => e
  log.error(e)
end
