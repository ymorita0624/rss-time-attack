# Rakuten Super Time Attack

## Description

seleniumで楽天の期間限定商品を高速で購入を目指すツール
※アクセス集中時のリトライ機能なし

## Requirements

- Ruby 2.3

## Installation

```
gem install selenium-webdriver
```

## Usage

1. config.sample.ymlをコピーしてconfig.ymlにリネーム
2. config.ymlにログイン情報や商品ページを入力
3. スクリプト実行

```
ruby main.rb
```
